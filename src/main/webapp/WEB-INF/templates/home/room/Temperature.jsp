<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body ng-app="homeapp">
<div id="cont">
    <div id="menu-fixed">
        <a href="#cont">
            <i class="material-icons back">&#xE314;</i>
        </a>
        <p><a href="/">KOTI</a></p>
        <a href="#menu-fixed">
            <div class="logo">
                <span></span>
            </div>
            <p class="pmenu">MENU</p>
        </a>
        <hr>
        <ul class="menu">
            <a href="user_profile"><li><i class="material-icons">&#xe7fd;</i><p>My Profile</p></li></a>
            <a href="my_homes"><li><i class="material-icons">&#xE88A;</i><p>My Homes</p></li></a>
            <a href="settings"><li><i class="material-icons">&#xE8B8;</i><p>Settings</p></li></a>
            <a href="logout"><li><i class="material-icons">&#xe879;</i><p>Logout</p></li></a>
        </ul>
    </div>
</div>
<div id="cont2">
    <div id="menu-fixed2">
        <a href="#cont2">
            <i class="material-icons back">&#xe315;</i>
        </a>
        <a href="#menu-fixed2">
            <div class="logo2">
                <span></span>
                <p></p>
            </div>
            <p class="pmenu2">NAVIGATION</p>
        </a>
        <hr>
        <ul class="menu2">
            <a href="temperature"><li><i class="material-icons">&#xe430;</i><p>Temperature</p></li></a>
            <a href="light"><li><i class="material-icons">&#xe90f;</i><p>Lights</p></li></a>
            <a href="air_cond"><li><i class="material-icons">&#xe915;</i><p>Air Conditioning</p></li></a>
        </ul>
    </div>
</div>
<nav>
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Adjust temperature in {{room.roomType}}</li>
    </ul>
</nav>
<div ng-controller="HomeController as ctrl">
    <div class="container">
        <div class="de">
            <div class="den">
                <hr class="line">
                <hr class="line">
                <hr class="line">
                <div class="switch">
                    <label for="switch_0"><span>18</span></label>
                    <label for="switch_1"><span>20</span></label>
                    <label for="switch_2"><span>22</span></label>
                    <label for="switch_3"><span>24</span></label>
                    <label for="switch_4"><span>26</span></label>
                    <label for="switch_5"><span>28</span></label>
                    <input id="switch_0" name="switch" ng-click="updateTemperature(1,18)" checked type="radio">
                    <input id="switch_1" name="switch" ng-click="updateTemperature(1,20)" type="radio">
                    <input id="switch_2" name="switch" ng-click="updateTemperature(1,22)" type="radio">
                    <input id="switch_3" name="switch" ng-click="updateTemperature(1,24)" type="radio">
                    <input id="switch_4" name="switch" ng-click="updateTemperature(1,26)" type="radio">
                    <input id="switch_5" name="switch" ng-click="updateTemperature(1,28)" type="radio">
                    <div class="light"><span></span></div>
                    <div class="dot"><span></span></div>
                    <div class="dene">
                        <div class="denem">
                            <div class="deneme">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
<script src="/static/js/app.js"></script>
<script src="/static/js/services/homes_service.js"></script>
<script src="/static/js/controller/homes_ctrlr.js"></script>
</body>
</html>