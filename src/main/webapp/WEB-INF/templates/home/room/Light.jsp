<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body ng-app="homeapp">
<div id="cont">
    <div id="menu-fixed">
        <a href="#cont">
            <i class="material-icons back">&#xE314;</i>
        </a>
        <p><a href="/">KOTI</a></p>
        <a href="#menu-fixed">
            <div class="logo">
                <span></span>
            </div>
            <p class="pmenu">MENU</p>
        </a>
        <hr>
        <ul class="menu">
            <a href="my_homes"><li><i class="material-icons">&#xE88A;</i><p>My Homes</p></li></a>
            <a href="logout"><li><i class="material-icons">&#xe879;</i><p>Logout</p></li></a>
        </ul>
    </div>
</div>
<div id="cont2">
    <div id="menu-fixed2">
        <a href="#cont2">
            <i class="material-icons back">&#xe315;</i>
        </a>
        <a href="#menu-fixed2">
            <div class="logo2">
                <span></span>
                <p></p>
            </div>
            <p class="pmenu2">NAVIGATION</p>
        </a>
        <hr>
        <ul class="menu2">
            <a href="temperature"><li><i class="material-icons">&#xe430;</i><p>Temperature</p></li></a>
            <a href="light"><li><i class="material-icons">&#xe90f;</i><p>Lights</p></li></a>
            <a href="air_cond"><li><i class="material-icons">&#xe915;</i><p>Air Conditioning</p></li></a>
        </ul>
    </div>
</div>
<nav>
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Adjust light in {{room.roomType}}</li>
    </ul>
</nav>
<div ng-controller="HomeController as ctrl">
    <div class="container">
        <div class="de">
            <div class="den">
                <hr class="line2">
                <hr class="line2">
                <div class="switch2">
                    <label for="switch_a"><span>Full</span></label>
                    <label for="switch_b"><span>Dim</span></label>
                    <label for="switch_c"><span>Dark</span></label>
                    <label for="switch_d"><span>Off</span></label>
                    <input id="switch_a" name="switch" ng-click="updateLight(1,'full')" checked type="radio">
                    <input id="switch_b" name="switch" ng-click="updateLight(1,'dim')" type="radio">
                    <input id="switch_c" name="switch" ng-click="updateLight(1,'dark')" type="radio">
                    <input id="switch_d" name="switch" ng-click="updateLight(1,'off')" type="radio">
                    <div class="light"><span></span></div>
                    <div class="dot"><span></span></div>
                    <div class="dene">
                        <div class="denem">
                            <div class="deneme">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
<script src="/static/js/app.js"></script>
<script src="/static/js/services/homes_service.js"></script>
<script src="/static/js/controller/homes_ctrlr.js"></script>
</body>
</html>