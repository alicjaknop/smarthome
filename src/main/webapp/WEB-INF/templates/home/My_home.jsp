<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body ng-app="homeapp">
<div id="cont">
    <div id="menu-fixed">
        <a href="#cont">
            <i class="material-icons back">&#xE314;</i>
        </a>
        <p><a href="/">KOTI</a></p>
        <a href="#menu-fixed">
            <div class="logo">
                <span></span>
            </div>
            <p class="pmenu">MENU</p>
        </a>
        <hr>
        <ul class="menu">
            <a href="my_homes"><li><i class="material-icons">&#xE88A;</i><p>My Homes</p></li></a>
            <a href="logout"><li><i class="material-icons">&#xe879;</i><p>Logout</p></li></a>
        </ul>
    </div>
</div>
<div id="cont2">
    <div id="menu-fixed2">
        <a href="#cont2">
            <i class="material-icons back">&#xe315;</i>
        </a>
        <a href="#menu-fixed2">
            <div class="logo2">
                <span></span>

            </div>
            <p class="pmenu2">MY HOMES</p>
        </a>
        <hr>
        <ul class="menu2">
            <a href="edit_home"><li><i class="material-icons">&#xe3c9;</i><p>Edit Home</p></li></a>
            <a href="add_home"><li><i class="material-icons">&#xe145;</i><p>Add Home</p></li></a>
            <a href="remove_home"><li><i class="material-icons">&#xe14c;</i><p>Remove Home</p></li></a>
        </ul>
    </div>
</div>

<div ng-controller="HomeController as ctrl">
    <div class="container5">
        <div ng-repeat="home in ctrl.homes">
            <nav>
                <ul>
                    <li><span ng-bind="home.name"></span></li>
                </ul>

                <div class="icons_a">
                    <a href="tv"><i class="material-icons">&#xe333;</i><span>TV</span></a>
                    <a href="locks"><i class="material-icons">&#xe897;</i><span>Locks</span></a>
                    <a href="alarm_clock"><i class="material-icons">&#xe190;</i><span>Alarm Clock</span></a>
                    <a href="internet_control"><i class="material-icons">&#xe328;</i><span>Internet Control</span></a>
                </div>

                <div class="icons">
                    <div ng-repeat="room in home.rooms">
                        <a href="temperature"><i class="material-icons">{{room.roomType.icon}}</i><span ng-bind="room.name"></span></a>

                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
<script src="/static/js/app.js"></script>
<script src="/static/js/services/homes_service.js"></script>
<script src="/static/js/controller/homes_ctrlr.js"></script>
</body>
</html>