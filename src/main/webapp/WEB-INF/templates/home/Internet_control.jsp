<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body ng-app="homeapp">
<div id="cont">
    <div id="menu-fixed">
        <a href="#cont">
            <i class="material-icons back">&#xE314;</i>
        </a>
        <p><a href="/">KOTI</a></p>
        <a href="#menu-fixed">
            <div class="logo">
                <span></span>
            </div>
            <p class="pmenu">MENU</p>
        </a>
        <hr>
        <ul class="menu">
            <a href="my_homes"><li><i class="material-icons">&#xE88A;</i><p>My Homes</p></li></a>
            <a href="logout"><li><i class="material-icons">&#xe879;</i><p>Logout</p></li></a>
        </ul>
    </div>
</div>
<nav class="inner">
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Remote Internet Control Panel</li>
    </ul>
</nav>
<div ng-controller="HomeController as ctrl">
    <div class="container">
        <div class="inner">
            <div class="de">
                <div class="den">
                    <hr class="line3">
                    <div class="switch3">
                        <label for="switch_on"><span>On</span></label>
                        <label for="switch_off"><span>Off</span></label>
                        <input id="switch_on" name="switch" ng-click="setInternet(1,true)" checked type="radio">
                        <input id="switch_off" name="switch" ng-click="setInternet(1,false)" type="radio">
                        <div class="light"><span></span></div>
                        <div class="dot"><span></span></div>
                        <div class="dene">
                            <div class="denem">
                                <div class="deneme">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p class="description">Number of devices that currently use this Network: <font color="#fff">#number</font></p>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
<script src="/static/js/app.js"></script>
<script src="/static/js/services/homes_service.js"></script>
<script src="/static/js/controller/homes_ctrlr.js"></script>
</body>
</html>