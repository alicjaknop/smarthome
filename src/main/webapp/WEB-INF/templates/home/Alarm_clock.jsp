<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body>
<div id="cont">
    <div id="menu-fixed">
        <a href="#cont">
            <i class="material-icons back">&#xE314;</i>
        </a>
        <p><a href="/">KOTI</a></p>
        <a href="#menu-fixed">
            <div class="logo">
                <span></span>
            </div>
            <p class="pmenu">MENU</p>
        </a>
        <hr>
        <ul class="menu">
            <a href="my_homes"><li><i class="material-icons">&#xE88A;</i><p>My Homes</p></li></a>
            <a href="logout"><li><i class="material-icons">&#xe879;</i><p>Logout</p></li></a>
        </ul>
    </div>
</div>
<nav class="inner">
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Remote Alarm Clock Control Panel</li>
    </ul>
</nav>
<div class="container2">
    <div class="clock-wrap">
        <div class="displays mode-display">24hr clock</div>
        <div class="displays ampm-display handle">am</div>
        <div class="clock-display">
            <ul class="control-hrs handle">
                <li>+</li>
                <li>-</li>
            </ul>
            <div class="hrs nums">00</div><span class="nums">:</span>
            <div class="mins nums">00</div>
            <ul class="control-mins handle">
                <li>+</li>
                <li>-</li>
            </ul>
        </div>
        <div class="displays alarm-display">no alarm set</div>
    </div>
    <div class="btn-wrap">
        <div class="alarm">set alarm</div>
        <div class="mode">mode</div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/static/js/style/alarm.js"></script>
</body>
</html>