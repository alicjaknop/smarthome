<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body>
<div id="cont">
    <div id="menu-fixed">
        <a href="#cont">
            <i class="material-icons back">&#xE314;</i>
        </a>
        <p><a href="/">KOTI</a></p>
        <a href="#menu-fixed">
            <div class="logo">
                <span></span>
            </div>
            <p class="pmenu">MENU</p>
        </a>
        <hr>
        <ul class="menu">
            <a href="my_homes"><li><i class="material-icons">&#xE88A;</i><p>My Homes</p></li></a>
            <a href="logout"><li><i class="material-icons">&#xe879;</i><p>Logout</p></li></a>
        </ul>
    </div>
</div>
<nav class="inner">
    <ul>
        <li>HomeName</li>
        <li class="subtitle">Remote TV Control Panel</li>
    </ul>
</nav>
<div class="container3" data-behaviour="search-on-list">
    <input type="text2" class="input-query" data-search-on-list="search" placeholder="Search channel..."/>
    <span class="counter" data-search-on-list="counter"></span>
    <div class="list-wrap">
        <ul class="list" data-search-on-list="list">
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP1</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP2</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Polsat</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Polsat FILM</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Polsat 2</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVN</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVN 7</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVN 24</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TV 4</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TV Puls</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP Historia</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP Rozrywka</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP Polonia</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>TVP Sport</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Tele 5</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>HBO</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>HBO 2</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Comedy Central</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>Animal Planet</span>
            </li>
            <li class="list-item" data-search-on-list="list-item">
                <span>MTV Polska</span>
            </li>
        </ul>
    </div>
<div class="container">
    <div class="toggle">
        <input type="checkbox">
        <span class="button"></span>
        <span class="label"><i class="material-icons">&#xe8ac;</i></span>
    </div>
</div>
<div class="container7">
    <span class="label"><i class="material-icons">&#xe050;</i></span>
    <input type="range" value="0.6" data-steps="8" id="sliderInput" />
</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="/static/js/style/search.js"></script>
<script src="/static/js/style/power_btn.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="/static/js/style/volume.js"></script>
</body>
</html>