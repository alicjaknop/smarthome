<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body>
<div class="focus">
    <div class="focus--mask">
        <div class="focus--mask-inner"><a href="/" class="Logo">KOTI</a></div>
    </div>
</div>
<div class="background"></div>
<div class="box_terms">
    <div class="skew-menu">
        <ul>
            <li><a href="terms">Introduction</a></li>
            <li><a href="terms_2">Property Rights</a></li>
            <li><a href="terms_3">Restrictions</a></li>
            <li><a href="terms_4">No Warranties</a></li>
            <li><a href="terms_5">Limitations</a></li>
            <li><a href="terms_6">Assignment</a></li>
        </ul>
    </div>
    <h1>Terms and Conditions</h1>
    <br>
    <h3>3. Restrictions</h3>
    <br><br>
    <p class="Terms"> You are expressly and emphatically restricted from all of the following:</p>
    <ol start="1">
        <p>1. Publishing any Website material in any media.</p>
        <p>2. Selling, sublicensing and/or otherwise commercializing any Website material.</p>
        <p>3. Publicly performing and/or showing any Website material.</p>
        <p>4. Using this Website in any way that is, or may be, damaging to this Website.</p>
        <p>5. Using this Website in any way that impacts user access to this Website.</p>
        <p>6. Using this Website contrary to applicable laws and regulations, or in a way that causes, or may cause, <br>harm to the Website, or to any person or business entity.</p>
    </ol>
    <p class="Terms"> Certain areas of this Website are restricted from access by You and KOTI&reg; may further restrict access by You to any areas of this Website, at any time, in its sole and absolute discretion. Any user ID and password You may have for this Website are confidential and You must maintain confidentiality of such information.</p>
    <br><br><br>
</div>
</body>
</html>