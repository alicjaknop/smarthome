<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body>
<div class="focus">
    <div class="focus--mask">
        <div class="focus--mask-inner"><a href="/" class="Logo">KOTI</a></div>
    </div>
</div>
<div class="background"></div>
<div class="box_terms">
    <div class="skew-menu">
        <ul>
            <li><a href="terms">Introduction</a></li>
            <li><a href="terms_2">Property Rights</a></li>
            <li><a href="terms_3">Restrictions</a></li>
            <li><a href="terms_4">No Warranties</a></li>
            <li><a href="terms_5">Limitations</a></li>
            <li><a href="terms_6">Assignment</a></li>
        </ul>
    </div>
    <h1>Terms and Conditions</h1>
    <br>
    <h3>4. No Warranties</h3>
    <br><br>
    <p class="Terms"> This Website is provided "as is", with all faults, and KOTI&reg; makes no express or implied representations or warranties, of any kind related to this Website or the materials contained on this Website. Additionally, nothing contained on this Website shall be construed as providing consult or advice to You.</p>
    <br><br><br>
</div>
</body>
</html>