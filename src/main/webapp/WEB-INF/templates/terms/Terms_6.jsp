<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body>
<div class="focus">
    <div class="focus--mask">
        <div class="focus--mask-inner"><a href="/" class="Logo">KOTI</a></div>
    </div>
</div>
<div class="background"></div>
<div class="box_terms">
    <div class="skew-menu">
        <ul>
            <li><a href="terms">Introduction</a></li>
            <li><a href="terms_2">Property Rights</a></li>
            <li><a href="terms_3">Restrictions</a></li>
            <li><a href="terms_4">No Warranties</a></li>
            <li><a href="terms_5">Limitations</a></li>
            <li><a href="terms_6">Assignment</a></li>
        </ul>
    </div>
    <h1>Terms and Conditions</h1>
    <br>
    <h3>6. Assignment</h3>
    <br><br>
    <p class="Terms"> KOTI&reg; shall be permitted to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification or consent required. However, You shall not be permitted to assign, transfer, or subcontract any of Your rights and/or obligations under these Terms.</p>
    <br><br><br>
</div>
</body>
</html>