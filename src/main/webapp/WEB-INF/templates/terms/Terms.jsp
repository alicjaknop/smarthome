<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body>
<div class="focus">
    <div class="focus--mask">
        <div class="focus--mask-inner"><a href="/" class="Logo">KOTI</a></div>
    </div>
</div>
<div class="background"></div>
<div class="box_terms">
    <div class="skew-menu">
        <ul>
            <li><a href="terms">Introduction</a></li>
            <li><a href="terms_2">Property Rights</a></li>
            <li><a href="terms_3">Restrictions</a></li>
            <li><a href="terms_4">No Warranties</a></li>
            <li><a href="terms_5">Limitations</a></li>
            <li><a href="terms_6">Assignment</a></li>
        </ul>
    </div>
    <h1>Terms and Conditions</h1>
    <br>
    <h3>1. Introduction</h3>
    <br><br>
    <p class="Terms"> These Website Standard Terms and Conditions (these "Terms" or these "Website Standard Terms and Conditions") contained herein on this webpage, shall govern Your use of this website, including all pages within this website (collectively referred to herein below as this "Website"). These Terms apply in full force and effect to Your use of this Website and by using this Website, You expressly accept all Terms and Conditions contained herein in full. You must not use this Website, if You have any objection to any of these Website Standard Terms And Conditions.</p><br><br>
    <p class="Terms"> This Website is not for use by any minors (defined as those who are not at least 18 years of age), and You must not use this Website if You are a minor.</p>
    <br><br><br>
</div>
</body>
</html>