<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/Header.jsp"%>
</head>
<body>
<div class="focus">
    <div class="focus--mask">
        <div class="focus--mask-inner"><a href="/" class="Logo">KOTI</a></div>
    </div>
</div>
<div class="background"></div>
<div class="box_terms">
    <div class="skew-menu">
        <ul>
            <li><a href="terms">Introduction</a></li>
            <li><a href="terms_2">Property Rights</a></li>
            <li><a href="terms_3">Restrictions</a></li>
            <li><a href="terms_4">No Warranties</a></li>
            <li><a href="terms_5">Limitations</a></li>
            <li><a href="terms_6">Assignment</a></li>
        </ul>
    </div>
    <h1>Terms and Conditions</h1>
    <br>
    <h3>2. Property Rights</h3>
    <br><br>
    <p class="Terms"> Other than content You own, which You may have opted to include on this Website, under these Terms, KOTI&reg; and/or its licensors own all rights to the intellectual property and material contained in this Website, and all such rights are reserved.</p><br><br>
    <p class="Terms"> You are granted a limited license only, subject to the restrictions provided in these Terms, for purposes of viewing the material contained on this Website.</p>
    <br><br><br>
</div>
</body>
</html>