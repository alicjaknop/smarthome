'use strict';

angular.module('homeapp').factory('HomesService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/rest/';

        var factory = {
            fetchAllHomes: fetchAllHomes,
            updateTemperature: updateTemperature,
            updateLight: updateLight,
            setAir: setAir,
            setLocks: setLocks,
            setInternet: setInternet,
            fetchRoom: fetchRoom,
            fetchAllRooms: fetchAllRooms
        };

        console.log('Factory created.');
        return factory;

        function updateTemperature(id, temp){
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'setRoomTemperature?roomId='+id+'&temperature='+temp).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function updateLight(id, light){
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'setRoomLight?roomId='+id+'&light='+light).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function setAir(id, air){
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'setAir?roomId='+id+'&air='+air).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function setLocks(id, locks){
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'setLocks?roomId='+id+'&locks='+locks).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function setInternet(id, internet){
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'setInternet?roomId='+id+'&internet='+internet).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchAllHomes() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'getHomes').then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchRoom(roomId, homeId) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'getRoom?roomId='+roomId+'&homeId='+homeId).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchAllRooms() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'getRoomsByHome').then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

    }]);