'use strict';

angular.module('homeapp').controller('SwitchingController', ['$scope', '$window',
    function ($scope, $window) {
        var self = this;
        self.enabledViewOne = false;
        self.enabledViewTwo = true;
        self.enabledView = 1;

        console.log('Controller created.');
        self.switchView = switchView;

        function switchView() {
            console.log('Switching.');
            if(self.enabledViewOne){
                self.enabledViewOne = false;
                self.enabledViewTwo = true;
            }else{
                self.enabledViewOne = true;
                self.enabledViewTwo = false;
            }
        }

        $scope.onloadFunction = function(id) {
            console.log('Fetch user with id:' + id);
            fetchUserWithId(id);
        }

    }]);