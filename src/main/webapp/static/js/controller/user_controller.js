'use strict';

angular.module('homeapp').controller('UserController', ['$scope', '$window', 'UserService',
    function ($scope, $window, UserService) {
        var self = this;
        self.users = [];
        self.user = {
            "id": null,
            "login": "loading",
            "passwordHash": "loading...",
            "homeList": [],
            "userData": {
                "id": null,
                "email": null,
                "female": false
            }
        };

        self.newUser = {
            "id": null,
            "login": "",
            "passwordHash": "",
            "homeList": [],
            "userData": {
                "id": null,
                "email": null,
                "female": false
            }
        };

        console.log('Controller created.');
        self.fetchAllUsersMethod = fetchAllUsers;
        self.moveFunction = moveFunction;
        self.sendNewUser = sendNewUser;
        self.fetchUserWithIdMethod = fetchUserWithId;
        self.addSomethingToSession = addSomethingToSession;
        // self.loadFromSession = loadFromSession;
        self.showMeWhatIsInSession = showMeWhatIsInSession;

        fetchAllUsers();

        // loadFromSession();

        function showMeWhatIsInSession() {
            var sessionCookieContent = $cookies.get('session_cookie');
            alert('' + sessionCookieContent);
        }

        function addSomethingToSession() {
            var variableWithValue = document.getElementById('sessionValue').value;
            console.log('Writing ' + variableWithValue + ' to session.');
            var a = new Date();
            a = new Date(a.getTime() + 30000);
            console.log('Expiration date: ' + a.toGMTString());
            $cookies.put('session_cookie', variableWithValue, {expires: '' + a.toGMTString()});
        }

        function moveFunction(userId) {
            console.log('This function.');
            $window.location.href = '/my_homes?userId=' + userId;
        }

        function fetchAllUsers() {
            UserService.fetchAllUsers().then(
                function (response) {
                    console.log(response);
                    self.users = response.data;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function fetchUserWithId(userId) {
            console.log('Fetch user with id: ' + userId);
            UserService.fetchUserWithId(userId).then(
                function (response) {
                    console.log(response);
                    self.user = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function sendNewUser() {
            console.log('Sending new user request. Login: ' + self.newUser.login);
            UserService.registerUser(self.newUser).then(
                function (response) {
                    console.log("User registered.");
                    $window.location.href = '/my_homes?userId=' + response.data.result.resultObject;
                },
                function (result) {
                    console.log("Unable to register user.");
                    console.log(result);
                }
            );
        }

        $scope.onloadFunction = function (id) {
            console.log('Fetch user with id: ' + id);
            fetchUserWithId(id);
        }

    }]);