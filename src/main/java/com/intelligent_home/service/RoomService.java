package com.intelligent_home.service;

import com.intelligent_home.model.Controller;
import com.intelligent_home.model.Room;
import com.intelligent_home.model.Sensor;

import java.util.List;

public interface RoomService {

    List<Long> getAllRoomsByUser(long userId);

    Room getRoom(long roomId);

    List<Sensor> getRoomSensors(long roomId);

    List<Controller> getRoomControllers(long roomId);

    Sensor getSensor(long sensorId);

    Controller getController(long controllerId);

    boolean roomExists(String roomName);

    boolean addRoom(long userId, long homeId, Room room);

    boolean addController(long userId, long roomId, Controller controller);

    boolean addSensor(long userId, long roomId, Sensor sensor);

}
