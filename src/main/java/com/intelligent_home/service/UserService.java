package com.intelligent_home.service;

import com.intelligent_home.model.User;
import com.intelligent_home.model.UserData;

import java.util.List;
import java.util.Optional;

public interface UserService {

    boolean userExists(String login);

    boolean registerUser(User userToRegister, UserData data);

    List<User> getAllUsers();

    boolean userLogin(String login, String password);

    Optional<User> getUserWithId(Long id);

//     void addTransaction(Long id, Long otherUserId);
}
