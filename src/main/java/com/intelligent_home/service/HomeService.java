package com.intelligent_home.service;

import com.intelligent_home.model.Home;
import com.intelligent_home.model.Room;

import java.util.List;

public interface HomeService {

    boolean homeExists(String homeName);

    List<Home> getAllHomes();

    List<Long> getHomeRooms(long homeId);

    Home getHomeById(long homeId);

    boolean addHome(long userId, Home home);

    void updateRoomTemperature(long roomId, int temperature);

//    boolean addRoom(long userId, long homeId, Room room);

    void updateRoomLight(long roomId, String light);

    void setAir(long roomId, boolean air);

    void setLocks(long roomId, boolean locks);

    void setInternet(long roomId, boolean internet);


}
