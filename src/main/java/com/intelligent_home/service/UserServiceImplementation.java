package com.intelligent_home.service;

import com.intelligent_home.dao.UserDao;
import com.intelligent_home.model.User;
import com.intelligent_home.model.UserData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service(value = "userService")
@Transactional
public class UserServiceImplementation implements UserService{

    @Autowired
    UserDao userDao;

    private static final Logger LOG = Logger.getLogger(UserServiceImplementation.class);

    @Override
    public boolean userExists(String login) {
        return userDao.userExists(login);
    }

    @Override
    public boolean registerUser(User userToRegister, UserData data) {
        userDao.registerUser(userToRegister, data);
        return true;
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public boolean userLogin(String login, String password) {
        throw new NotImplementedException();
    }

//    @Override
//    public void postSomething(String post, User sender) {
//    }

    @Override
    public Optional<User> getUserWithId(Long id) {
        return userDao.findById(id);
    }
//
//    @Override
//    public void addTransaction(Long id, Long otherUserId, Double amount) {
//        Optional<User> user = getUserWithId(id);
//        Optional<User> userTo = getUserWithId(otherUserId);
//        if(user.isPresent() && userTo.isPresent()){
//            User ufrom = user.get();
//            User uto = user.get();
//            userDao.persistTransaction(ufrom,uto, new Transaction(ufrom, uto, amount));
//        }
//    }

}
