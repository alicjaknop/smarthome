package com.intelligent_home.configuration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class HomesDataInitializer {
    @Bean
    InitializingBean sendDatabase() {
        return () -> {
            Logger.getLogger(getClass()).info("Completed creating bean.");

        };
    }
}