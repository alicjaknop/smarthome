package com.intelligent_home.dao;

import com.intelligent_home.model.User;
import com.intelligent_home.model.UserData;
import com.sun.istack.internal.NotNull;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    void registerUser(User u, UserData data);

    Optional<User> findById(@NotNull Long id);

    boolean userExists(@NotNull String withLogin);

    List<User> getAllUsers();

    void save(User userTransferring);

    Optional<User> findByLogin(String login);
}
