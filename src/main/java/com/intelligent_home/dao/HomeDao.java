package com.intelligent_home.dao;

import com.intelligent_home.model.Home;
import com.intelligent_home.model.Room;
import com.sun.istack.internal.NotNull;

import java.util.List;

public interface HomeDao {
    List<Home> getAllHomes();

    Home getHomeById(long homeId);

    public void updateRoom (Room room);

    public Room getRoomById(long id);

    void addHome(long userId, Home home);

    boolean homeExists(@NotNull String homeName);


}
