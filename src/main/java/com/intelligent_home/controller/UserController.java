package com.intelligent_home.controller;

import com.intelligent_home.model.User;
import com.intelligent_home.model.UserData;
import com.intelligent_home.model.responses.Response;
import com.intelligent_home.model.responses.ResponseFactory;
import com.intelligent_home.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/")
public class UserController {

    @Autowired
    UserService service;

    @RequestMapping(value = "/registerUserWithData", method = RequestMethod.POST)
    public ResponseEntity<Response> registerUserWithData(@RequestBody User user) {
        Logger.getLogger(getClass()).debug("Requested registerUser with user: " + user);
        if (service.userExists(user.getLogin())) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User exists"),
                    HttpStatus.BAD_REQUEST);
        } else {
            service.registerUser(user, user.getUserData());
            return new ResponseEntity<Response>(
                    ResponseFactory.success(user.getId()),
                    HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/testUser", method = RequestMethod.GET)
    public ResponseEntity<User> requestTestUser() {
        return new ResponseEntity<User>(
                new User("login", "hash"), HttpStatus.OK);
    }

    @RequestMapping(value = "/registerUser", method = RequestMethod.GET)
    public ResponseEntity<Response> registerUser(@RequestParam String login,
                                                 @RequestParam String passwordHash) {
        Logger.getLogger(getClass()).debug("Requested registerUser with params:" +
                " " + login + ":" + passwordHash);

        if (service.userExists(login)) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User exists"),
                    HttpStatus.BAD_REQUEST);
        } else {
            service.registerUser(new User(login, passwordHash), new UserData());
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/listUsers", method = RequestMethod.GET)
    public ResponseEntity<List<User>> requestUserList() {
        return new ResponseEntity<List<User>>(service.getAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> getUserWithId(@PathVariable Long id) {
        Optional<User> user = service.getUserWithId(id);
        if (user.isPresent()) {
            return new ResponseEntity<Response>(ResponseFactory.success(user.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with this id does not exist."), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/userExists/{login}", method = RequestMethod.GET)
    public ResponseEntity<Response> requestUserExists(@PathVariable String login) {
        return new ResponseEntity<Response>(ResponseFactory.success(service.userExists(login)), HttpStatus.OK);
    }

}
