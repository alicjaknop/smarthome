package com.intelligent_home.controller;

import com.intelligent_home.model.*;
import com.intelligent_home.model.responses.Response;
import com.intelligent_home.model.responses.ResponseFactory;
import com.intelligent_home.service.HomeService;
import com.intelligent_home.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest")
public class HomeController {


    @Autowired
    HomeService homeService;
    RoomService roomService;

    @RequestMapping(value = "/getHomes", method = RequestMethod.GET)
    public ResponseEntity<Response> getHomes(){
        return new ResponseEntity<Response>(ResponseFactory.success(homeService.getAllHomes()), HttpStatus.OK);
    }

    @RequestMapping(value = "/getRoomsByUser", method = RequestMethod.GET)
    public ResponseEntity<List<Long>> getRoomsByUser(@RequestParam long userId){
        return new ResponseEntity<List<Long>>(roomService.getAllRoomsByUser(userId), HttpStatus.OK);
    }

    @RequestMapping(value = "/getRoom", method = RequestMethod.GET)
    public ResponseEntity<Response> getRoom(@RequestParam long roomId){
        return new ResponseEntity<Response>(ResponseFactory.success(roomService.getRoom(roomId)), HttpStatus.OK);
    }

    @RequestMapping(value = "/getRoomSensors", method = RequestMethod.GET)
    public ResponseEntity<Response> getRoomSensors(@RequestParam long roomId){
        return new ResponseEntity<Response>(ResponseFactory.success(roomService.getRoomSensors(roomId)), HttpStatus.OK);
    }

    @RequestMapping(value = "/getRoomControllers", method = RequestMethod.GET)
    public ResponseEntity<Response> getRoomControllers(@RequestParam long roomId){
        return new ResponseEntity<Response>(ResponseFactory.success(roomService.getRoomControllers(roomId)), HttpStatus.OK);
    }

    @RequestMapping(value = "/getSensor", method = RequestMethod.GET)
    public ResponseEntity<Response> getSensor(@RequestParam long sensorId){
        return new ResponseEntity<Response>(ResponseFactory.success(roomService.getSensor(sensorId)), HttpStatus.OK);
    }

    @RequestMapping(value = "/getController", method = RequestMethod.GET)
    public ResponseEntity<Response> getController(@RequestParam long controllerId){
        return new ResponseEntity<Response>(ResponseFactory.success(roomService.getController(controllerId)), HttpStatus.OK);
    }

    @RequestMapping(value = "/getRoomsByHome", method = RequestMethod.GET)
    public ResponseEntity<Response> getRoomsByHome(@RequestParam long homeId){
        return new ResponseEntity<Response>(ResponseFactory.success(homeService.getHomeRooms(homeId)), HttpStatus.OK);
    }

    @RequestMapping(value = "/homeExists/{homeName}", method = RequestMethod.GET)
    public ResponseEntity<Response> homeExists(@PathVariable String homeName){
        return new ResponseEntity<Response>(ResponseFactory.success(homeService.homeExists(homeName)), HttpStatus.OK);
    }

    @RequestMapping(value = "/addHome", method = RequestMethod.GET)
    public ResponseEntity<Response> addHome(@RequestParam long userId, @RequestParam String homeName){
        if(homeService.homeExists(homeName)){
            return new ResponseEntity<Response>(ResponseFactory.failed("Home already exists."), HttpStatus.BAD_REQUEST);
        }else{
            Home home = new Home();
            home.setName(homeName);
            return new ResponseEntity<Response>(ResponseFactory.success(homeService.addHome(userId, home)), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/roomExists/{roomName}", method = RequestMethod.GET)
    public ResponseEntity<Response> roomExists(@PathVariable String roomName){
        return new ResponseEntity<Response>(ResponseFactory.success(roomService.roomExists(roomName)), HttpStatus.OK);
    }

    @RequestMapping(value = "/addRoom", method = RequestMethod.GET)
    public ResponseEntity<Response> addRoom(@RequestParam long userId, @RequestParam long homeId, @RequestParam String roomName, @RequestParam RoomType roomType){
        if(roomService.roomExists(roomName)){
            return new ResponseEntity<Response>(ResponseFactory.failed("Room already exists."), HttpStatus.BAD_REQUEST);
        }else{
            Room room = new Room();
            room.setName(roomName);
            room.setRoomType(roomType);
            return new ResponseEntity<Response>(ResponseFactory.success(roomService.addRoom(userId, homeId, room)), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/addController", method = RequestMethod.GET)
    public ResponseEntity<Response> addController(@RequestParam long userId, @RequestParam long roomId){
        Controller controller = new Controller();
        return new ResponseEntity<Response>(ResponseFactory.success(roomService.addController(userId, roomId, controller)), HttpStatus.OK);
    }

    @RequestMapping(value = "/addSensor", method = RequestMethod.GET)
    public ResponseEntity<Response> addSensor(@RequestParam long userId, @RequestParam long roomId){
        Sensor sensor = new Sensor();
        return new ResponseEntity<Response>(ResponseFactory.success(roomService.addSensor(userId, roomId, sensor)), HttpStatus.OK);
    }

    @RequestMapping(value = "/setRoomTemperature", method = RequestMethod.GET)
    public ResponseEntity<Response> setTemperature(@RequestParam long roomId, @RequestParam int temperature){
        homeService.updateRoomTemperature(roomId, temperature);
        return new ResponseEntity<Response>(HttpStatus.OK);
    }

    @RequestMapping(value = "/setRoomLight", method = RequestMethod.GET)
    public ResponseEntity<Response> setLight(@RequestParam long roomId, @RequestParam String light){
        homeService.updateRoomLight(roomId, light);
        return new ResponseEntity<Response>(HttpStatus.OK);
    }

    @RequestMapping(value = "/setAir", method = RequestMethod.GET)
    public ResponseEntity<Response> setAir(@RequestParam long roomId, @RequestParam boolean air){
        homeService.setAir(roomId, air);
        return new ResponseEntity<Response>(HttpStatus.OK);
    }

    @RequestMapping(value = "/setLocks", method = RequestMethod.GET)
    public ResponseEntity<Response> setLocks(@RequestParam long roomId, @RequestParam boolean locks){
        homeService.setLocks(roomId, locks);
        return new ResponseEntity<Response>(HttpStatus.OK);
    }

    @RequestMapping(value = "/setInternet", method = RequestMethod.GET)
    public ResponseEntity<Response> setInternet(@RequestParam long roomId, @RequestParam boolean internet){
        homeService.setInternet(roomId, internet);
        return new ResponseEntity<Response>(HttpStatus.OK);
    }
}













