package com.intelligent_home.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "user_data")
public class UserData {
    @Id
    @Column
    @GeneratedValue
    private long id;
    @Column
    private String login;
    @Column
    private String email;



    public UserData() {
        this.id = 0L;
    }

    public UserData(String login, String email, boolean isFemale) {
        this.id = 0L;
        this.email = email;}


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLastName(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



}
