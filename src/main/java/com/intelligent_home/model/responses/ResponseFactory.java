package com.intelligent_home.model.responses;

public class ResponseFactory {
    public static Response success() {
        return new Response();
    }

    public static Response success(Object resultObject) {
        return new Response(new Result(resultObject, "OK"));
    }

    public static Response failed(String message) {
        return new Response(new Result(message));
    }

    public static Response failed(String message, Object resultObject) {
        return new Response(new Result(resultObject, message));
    }
}
