package com.intelligent_home.model.responses;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Result {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Object resultObject;
    protected String message;

    public Result() {
        this.message = "OK";
    }

    public Result(Object resultObject) {
        this.resultObject = resultObject;
    }

    public Result(Object resultObject, String message) {
        this.resultObject = resultObject;
        this.message = message;
    }

    public Result(String reason, String message) {
        this.resultObject = null;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResultObject() {
        return resultObject;
    }

    public void setResultObject(Object resultObject) {
        this.resultObject = resultObject;
    }
}
