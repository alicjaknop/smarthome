package com.intelligent_home.model;

import javax.persistence.*;

@Entity
@Table
public class Trial {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;
}
