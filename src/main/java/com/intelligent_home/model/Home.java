package com.intelligent_home.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 * Time: 18:08
 */

@Entity
@Table(name = "home")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Home {
    @Id
    @GeneratedValue
    private long id;
    @Column
    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Room>  rooms = new ArrayList<>();

    public Home() {
        this.id=id;
    }

    public Home(long id, String name, List<Room> rooms) {
        this.id = id;
        this.name = name;
        this.rooms = rooms;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    @Override
    public String toString() {
        return "Home{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}