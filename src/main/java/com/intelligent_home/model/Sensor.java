package com.intelligent_home.model;

import javax.persistence.*;

/**
 * Project: IntelligentHome
 * Author: Anna Żukowska
 * Date: 2017-12-21
 * Time: 18:09
 */

@Entity
@Table
public class Sensor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Sensor() {
    }

    public Sensor(Long id) {
        this.id = 0L;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = 0l;
    }
}