package com.intelligent_home.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "room")
public class Room {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Sensor> sensors = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Controller> controllers = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    private RoomType roomType;

    @Column
    private int temperature;

    @Column
    private String light;

    @Column
    private boolean air;

    @Column
    private boolean locks;

    @Column
    private boolean internet;

    public Room() {
    }

    public Room(long id, String name, List<Sensor> sensors, List<Controller> controllers, RoomType type, int temperature, String light, boolean air, boolean locks, boolean internet) {
        this.id = id;
        this.name = name;
        this.sensors = sensors;
        this.controllers = controllers;
        this.roomType = type;
        this.temperature = temperature;
        this.light = light;
        this.air = air;
        this.locks = locks;
        this.internet = internet;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    public List<Controller> getControllers() {
        return controllers;
    }

    public void setControllers(List<Controller> controllers) {
        this.controllers = controllers;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public boolean getAir() {
        return air;
    }

    public void setAir(boolean air) {
        this.air = air;
    }

    public boolean getLocks() {
        return locks;
    }

    public void setLocks(boolean locks) {
        this.locks = locks;
    }

    public boolean getInternet() {
        return internet;
    }

    public void setInternet(boolean internet) {
        this.internet = internet;
    }
}
