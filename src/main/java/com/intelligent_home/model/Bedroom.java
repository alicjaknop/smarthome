package com.intelligent_home.model;

import javax.persistence.*;


@Entity
@Table
public class Bedroom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Bedroom() {
    }

    public Bedroom(Long id) {
        this.id=id;

    }


    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
